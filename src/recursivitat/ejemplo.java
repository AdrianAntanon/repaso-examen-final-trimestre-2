package recursivitat;

public class ejemplo {
    public static void main(String[] args) {
        ejemplo programa = new ejemplo();

        programa.inici();
    }

    public void inici(){
        System.out.println(factorial(5));
    }

    public int factorial (int n){
        if (n == 0){
            System.out.println("Caso base: se evalua a 0");
            return 1;
        }else{
            System.out.println("Caso recursivo: " + (n-1)+ ": se invoca el factorial");
            int nada = n * factorial(n-1);
            System.out.println("Caso recursivo " + (n-1) + ": resultado = " + nada);
            return nada;

        }
    }
}
