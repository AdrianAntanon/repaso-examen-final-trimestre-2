package recursivitat;

import java.util.Scanner;

/**
 Una fórmula interessant i que es pot deduir amb eines avançades de càlcul és la fórmula de
 Wallis, que permet tenir una expressió del número p a partir d'aquests productes:
 pi = 2 * 2/1 * 2/3 * 4/3 * 4/5 * 6/5 * 6/7 * 8/7 * 8/9
 */

public class ejercicio_4 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio_4 programa = new ejercicio_4();
        programa.inici();
    }

    public void inici(){

        int numero = introducirNum();
        System.out.printf("El resultado es %.2f",calcular_wallis(numero));

    }

    public int introducirNum(){
        System.out.println("Introduce un número, por favor");
        comprobacion();
        int num = lector.nextInt();

        return num;
    }

    /**
     @description: wallis
     @param: a number
     @returns: wallis product
     */

    public double calcular_wallis(int numero){

        if (numero == 0){
            return 4;
        }

        if (numero%2==0){
            return calcular_wallis(numero-1)*(double)numero/(numero+1);
        }else {
            return calcular_wallis(numero-1)*(double)(numero+1)/numero;
        }

    }



    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número entero, vuelve a introducirlo");
            lector.next();
        }
    }

}
