package recursivitat;

public class invertirPalabra {
    public static void main(String[] args) {
        invertirPalabra programa = new invertirPalabra();
        programa.inici();
    }
    public void inici(){
        String palabra = "Programa";
        System.out.println(palabraInvertida(palabra, palabra.length()-1));
    }

    public String palabraInvertida(String palabra, int longitud_palabra){
        if(longitud_palabra==0){
            return palabra.charAt(longitud_palabra)+"";
        }else{
            return palabra.charAt(longitud_palabra) + (palabraInvertida(palabra, longitud_palabra-1));
        }

    }
}
