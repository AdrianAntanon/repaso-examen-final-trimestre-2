package funciones_con_parametros;

import java.util.Scanner;
/*
Feu un programa anomenat NumeroPrimer que inclogui un mètode anomenat esPrimer.
Aquest mètode, donat un número enter, ha de dir si és primer o no. Un número primer és
aquell que no pot ser dividit per cap altre número excepte l’1 o ell mateix. Només cal que
funcioni amb valors positius, però no es pot usar System.out.println(...) dins d’aquest mètode.
Proveu que funciona fent successives invocacions des del mètode inici.
A mode d’exemple, si es prova pels valors 18, 13 i 33, aquest programa podria mostrar per
pantalla el següent:

El 18 és primer? false
El 13 és primer? true
El 33 és primer? false
*/

public class ejercicio2_numPrimo {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio2_numPrimo programa = new ejercicio2_numPrimo();

        programa.inici();
    }

    public void inici(){
        boolean acabar = false;

        do {

            lectura();
            acabar=terminar(acabar);

        }while (!acabar);

        System.out.println("Saliendo del programa");

    }

    public boolean terminar(boolean acabar){
        System.out.println("¿Quieres terminar? (si = salir)");
        String finalizar = lector.next().toLowerCase();
        if (finalizar.equals("si")) acabar = true;

        return acabar;
    }

    public void lectura(){
        int numPositivo;

        System.out.println("Introduce un número entero y positivo, por favor");
        comprobacion();

        numPositivo = lector.nextInt();

        while (numPositivo<1){
            System.out.println("El número debe ser positivo, vuelva a introducirlo");

            comprobacion();

            numPositivo=lector.nextInt();
        }

        System.out.println(esPrimer(numPositivo));

    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número válido, vuelve a introducirlo, por favor");
            lector.next();
        }
    }

    public String esPrimer(int num){
        boolean resultado = true;

        if (num%2==1) return "El número " + num + " es impar? " + resultado;
        else return "El número " + num + " es impar? " + !resultado;
    }

}
