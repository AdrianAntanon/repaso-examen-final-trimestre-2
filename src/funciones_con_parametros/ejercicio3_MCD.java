package funciones_con_parametros;

import java.util.Scanner;
/*
L’objectiu d’aquesta activitat és saber codificar un mètode amb paràmetres d’entrada i sortida
perquè realitzi una tasca concreta.
Donat el codi font parcial del programa MaximComuDivisor, que es mostra tot seguit,
completeu el mètode mcd (inicialment buit), de manera que, donats dos valors enters (positius
o negatius), calculi el seu Màxim Comú Divisor (MCD) seguit l'algorisme d'Euclides.
*/


public class ejercicio3_MCD {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio3_MCD programa = new ejercicio3_MCD();

        programa.inici();
    }

    public void inici(){
        System.out.println("Valor de A");
        int num1 = leerEntero();
        System.out.println("Valor de B");
        int num2 = leerEntero();

        System.out.println("El máximo común divisor de " + num1 + " y " + num2 + " es " + mcd(num1,num2));
    }

    public int leerEntero(){
        System.out.println("Introduce un número entero, por favor");
        comprobacion();
        int num = lector.nextInt();

        return num;
    }

    public int mcd(int a, int b) {

        while(a != b){
            if(a>b) a= a-b;
            else b = b -a;
        }

        int MCD = a;
        return MCD;
    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número válido, vuelve a introducirlo, por favor");
            lector.next();
        }
    }
}
