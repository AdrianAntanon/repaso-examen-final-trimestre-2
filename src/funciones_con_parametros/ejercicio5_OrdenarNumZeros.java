package funciones_con_parametros;
/*
L’objectiu d’aquesta activitat és crear un programa basat en invocacions a mètodes
amb paràmetres d’entrada i de sortida.
Feu un programa anomenat OrdenarPerZeros que, donat un array de números enters
inicialitzat amb valors concrets, primer el mostri per pantalla i després el mostri
ordenat de manera ascendent d’acord al nombre de 0 de cada valor. Declareu l’array
com variable dins el mètode inici. A mode d’exemple, el comportament del programa
podria ser el següent:
[ 100 2014 12 30 302 40020 7009 500000 ]
[ 500000 40020 7009 100 2014 30 302 12 ]
*/

public class ejercicio5_OrdenarNumZeros {
    public static void main(String[] args) {
        ejercicio5_OrdenarNumZeros programa = new ejercicio5_OrdenarNumZeros();
        programa.inici();
    }

    public void inici(){
        int [] array = {10, 100, 1, 4000, 320, 23, 305, 30020, 1002030, 20301000};

        System.out.println("Array original");
        imprimirArray(array);

        System.out.println("Array ordenado por número de ceros");
        array = ordenacion(array, num_ceros(array));
        imprimirArray(array);
    }

    public int [] num_ceros (int [] vector){

        String [] prueba= new String[vector.length];
        int [] numCeros= new int[vector.length];

        for (int i=0;i<vector.length;i++){
            prueba[i]= String.valueOf(vector[i]);
            if (prueba[i].contains("0")){
                for (int j=0;j<prueba[i].length();j++){
                    if (prueba[i].charAt(j) == '0'){
                        numCeros[i]++;
                    }
                }
            }
        }

        return numCeros;
    }


    public void imprimirArray (int [] lista){

        System.out.print("[ ");
        for(int i=0;i<lista.length;i++){
            System.out.print(lista[i] + " ");
        }
        System.out.println("]");
    }

    public int [] ordenacion( int [] valores, int [] criterioOrdenacion){

        for (int i = 0; i < criterioOrdenacion.length - 1; i++) {
            for(int j = i + 1; j < criterioOrdenacion.length; j++) {
                if (criterioOrdenacion[i] < criterioOrdenacion[j]) {
                    int cero = criterioOrdenacion[i];
                    criterioOrdenacion[i] = criterioOrdenacion[j];
                    criterioOrdenacion[j] = cero;

                    int numeros = valores[i];
                    valores[i] = valores[j];
                    valores[j] = numeros;
                }
            }
        }


        return valores;
    }
}
