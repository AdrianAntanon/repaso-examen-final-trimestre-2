package funciones_con_parametros;

import java.util.Scanner;
/*
L’objectiu d’aquesta activitat és crear un programa basat en invocacions a mètodes
amb paràmetres d’entrada i de sortida.
Feu un programa anomenat MesVocals que llegeixi una frase i, tot seguit, mostri per
pantalla quina és la paraula amb més vocals (sense importar majúscules i minúscules).
A mode d’exemple, el comportament del programa podria ser el següent:
Escriu una frase:
Hi havia una vegada una noia anomenada Caputxeta Vermella
La frase amb mes vocals és "anomenada".
*/

public class ejercicio6_MasVocales {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio6_MasVocales programa = new ejercicio6_MasVocales();

        programa.inici();
    }

    public void inici(){
        leerFrase();
    }

    public void leerFrase(){
        System.out.println("Introduce una frase, por favor");
        String frase = lector.nextLine().toLowerCase();

        String masVocales = contarVocales(frase);

        System.out.println("La palabra con más vocales es " + masVocales);
    }

    public String contarVocales(String a){
        String [] palabras = a.split(" ");
        int maxVocales=0;
        String masVocales = "";
        String vocales = "aeiou";

        for (int i=0;i<palabras.length;i++){
            int numVocales=0;
            for (int j=0;j<palabras[i].length();j++){
                for (int z=0;z<vocales.length();z++){
                    if (palabras[i].charAt(j) == vocales.charAt(z)){
                        numVocales++;
                    }
                }
                if (numVocales > maxVocales){
                    maxVocales = numVocales;
                    masVocales = palabras[i];
                }
            }
        }

        return masVocales;
    }

}
