package funciones_con_parametros;
/*
Donat el codi font parcial del programa DiaSetmana, que es mostra tot seguit, completeu
el mètode diaDeLaSetmana (inicialment buit), de manera que, donat un número de dia de
la setmana (1-7), retorni el nom del dia (Dilluns...Diumenge). També ha de tenir en
compte el cas que el valor d’entrada no sigui cap dia de la setmana.
*/
public class ejercicio1_diaSemana {
    public static void main(String[] args) {
        ejercicio1_diaSemana programa = new ejercicio1_diaSemana();

        programa.inici();
    }

    public void inici(){
        diaSemana();
    }

    public void diaSemana(){
        String [] dias_semana={"Lunes","Martes","Miércoles","Jueves","Viernes","Sábado","Domingo"};

        for (int i=0;i<=dias_semana.length;i++){
            System.out.print("El día " + (i+1)+ " de la semana es ");
            System.out.println(diaDeLaSetmana(i,dias_semana));

        }

    }

    public String diaDeLaSetmana(int numDia, String [] dias_semana) {

        String diaSemana = "";
        switch (numDia){
            case 0:
                diaSemana= dias_semana[numDia];
                break;
            case 1:
                diaSemana=dias_semana[numDia];
                break;
            case 2:
                diaSemana=dias_semana[numDia];
                break;
            case 3:
                diaSemana=dias_semana[numDia];
                break;
            case 4:
                diaSemana=dias_semana[numDia];
                break;
            case 5:
                diaSemana=dias_semana[numDia];
                break;
            case 6:
                diaSemana=dias_semana[numDia];
                break;
            default:
                diaSemana= "--> (no existe este día)";
        }
        return diaSemana;
    }



}
