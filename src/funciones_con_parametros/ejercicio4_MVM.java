package funciones_con_parametros;

import java.util.Scanner;
/*
L’objectiu d’aquesta activitat és saber usar invocacions a mètodes per treballar amb valors dins
un array.
Feu un programa anomenat MitjanaTresMaxims que llegeixi una seqüència de 10 valors enters
des del teclat, els desi a un array i mostri per pantalla quina és la mitjana aritmètica dels seus
tres valors més grans. A mode d’exemple, el comportament del programa podria ser el
següent:
Escriu deu enters separats per espais:
10 3 5 8 12 3 5 0 11 1
La mitjana dels tres valors més grans és 11.0
*/

public class ejercicio4_MVM {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio4_MVM programa = new ejercicio4_MVM();

        programa.inici();
    }

    public void inici(){
        System.out.printf("La media de los 3 valores más altos es %.2f ", calcularMedia(solicitud()));
    }

    public int [] solicitud(){
        int [] array = new int[10];
        System.out.println("Introduce 10 enteros separados por un espacio, por favor");

        for (int i=0;i<array.length;i++){
            comprobacion(i);
            array[i] = lector.nextInt();
        }

        ordenacion(array);

        return array;
    }

    public double calcularMedia(int [] a){
        double media=0;

        for (int i=9;i>=7;i--){
            media = media + a[i];
        }

        media=media/3;
        return  media;
    }

    public void ordenacion(int [] array){
        for (int i = 0; i < array.length - 1; i++) {
            for(int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int canvi = array[i];
                    array[i] = array[j];
                    array[j] = canvi;
                }
            }
        }
    }

    public void comprobacion(int i){
        while (!lector.hasNextInt()){
            System.out.println("El valor introducido en la posición " + (i+1) + " no es correcto, vuelve a introducirlo, por favor");
            lector.next();
        }
    }
}
