package funciones;

import java.util.Scanner;
/*
Feu un programa anomenat CalculTirada que calculi la probabilitat en tant per cent de treure per sota d’un valor concret tirant dos daus de sis cares.
Per fer això, l’estratègia que s’ha considerat és desar dins d’un array el nombre de repeticions de cada tirada possible (sempre entre 2 i 12) i després treballar a partir d’aquests valors, sabent que:

Probabilitat de treure 2: suma del nombre de tirades que valen 2 * 100 / 36.
Probabilitat de treure 3 o menys: suma del nombre de tirades que valen 2 o 3 * 100 / 36.
Probabilitat de treure 4 o menys: suma del nombre de tirades que valen 2 o 3 o 4 * 100 / 36.
Es parteix de la descomposició del problema següent, que haureu de seguir per codificar-lo:

Llegir el valor.
Processar entrada pel teclat.
Comprovar si està entre 2 i 12.
Generar tirades.
Calcular probabilitat.
A mode de guia, la sortida hauria de ser més o menys com es mostra tot seguit:

Escriu el valor a calcular [2 - 12].
18
El valor no és entre 2 i 12.
5
La probabilitat es 27.0%.
*/


public class ejercicio3 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio3 programa = new ejercicio3();

        programa.inici();
    }

    public void inici(){

        solicitudValor();

    }

    public void solicitudValor(){

        System.out.println("Introduce el número de resultado que te gustaría obtener en una tirada, por favor.\n" +
                "Recuerde que solo es posible sacar entre 2 y 12.");

        comprobarInt();

        int resultadoDados=lector.nextInt();

        resultadoDados = comprobacionValor(resultadoDados);

        generarTirades(resultadoDados);

    }

    public void generarTirades(int resultadoDados){
        double valoresTiradas = 0;
        int []posibilidades = {1, 2, 3, 4, 5, 6, 5, 4, 3, 2, 1};
        int j=0;
        for (int i=1;i<resultadoDados;i++){
            if (i<resultadoDados){
                valoresTiradas=posibilidades[j]+valoresTiradas;
            }
            j++;
        }

        mostrarResultados(resultadoDados, valoresTiradas);

    }

    public int comprobacionValor(int resultadoDados){
        while (resultadoDados<2 || resultadoDados>12){
            System.out.println("El valor debe de ser entre 2 y 12, vuelve a introducirlo, por favor");
            while (!lector.hasNextInt()){
                System.out.println("Ese valor no está permitido, vuelve a introducirlo, por favor");
                lector.next();
            }
            resultadoDados=lector.nextInt();
        }

        return resultadoDados;
    }

    public void mostrarResultados(int resultadoDados, double valoresTiradas){
        System.out.print("El porcentaje de probabilidades de salir un resultado de "+ resultadoDados +" o inferior es del ");
        System.out.printf("%.2f", (valoresTiradas*100)/36);
        System.out.println("%");
    }

    public void comprobarInt(){
        while (!lector.hasNextInt()){
            System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
            lector.next();
        }
    }
}
