package funciones;

import java.util.Scanner;
/*
Feu un programa anomenat FraseAmbMesAs que vagi llegint frases pel teclat i, en acabar cada entrada d’una frase,
mostri quina és la frase que s’ha escrit fins al moment amb més lletres ‘a’ (minúscula o majúscula) i quantes en tenia.
El programa ha d’anar-se executant fins a llegir la frase “fi”.*/

public class ejercicio1 {
    Scanner lector = new Scanner(System.in);
    private boolean acabar=false;
    private int numAs=0;
    private String fraseMasAs;

    public static void main(String[] args) {
        ejercicio1 programa = new ejercicio1();

        programa.inici();
    }

    public void inici(){

        do {
            recogerFrase();
        }while (!acabar);

        mostrarResultado();
    }

    public void recogerFrase(){
        System.out.println("Introduce una frase, escribre -->fin<-- si deseas terminar");
        String frase = lector.nextLine().toLowerCase();

        if (frase.equals("fin")) acabar = true;
        else contarNumAs(frase);
    }

    public void contarNumAs(String frase){

        int contador = 0;

        for (int i=0;i<frase.length();i++){
            if (frase.charAt(i) == 'a'){
                contador++;
            }
        }

        if (contador>numAs){
            numAs=contador;
            fraseMasAs=frase;
        }
    }

    public void mostrarResultado(){

        System.out.println("La frase que contiene más A's es --> " + fraseMasAs + " <--, que en total tiene " + numAs+" vocales A");
    }



}
