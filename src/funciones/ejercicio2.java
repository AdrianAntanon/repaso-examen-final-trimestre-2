package funciones;

import java.util.Scanner;
/*
Feu un programa anomenat GenerarHistograma que mostri l’histograma de totes les
tirades possibles amb dos daus de sis cares. Per fer això, l’estratègia que
s’ha considerat és desar dins d’un array el nombre de repeticions de cada
tirada possible (sempre entre 2 i 12) i després treballar a partir d’aquests
valors. A continuació, ha de dir quin valor de tirada és el que té més
repeticions. A mode de guia, la sortida hauria de ser semblant a la que es
mostra tot seguit:
*/

public class ejercicio2 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio2 programa = new ejercicio2();

        programa.inici();
    }

    public void inici(){
        solicitarLanzamientos();
    }

    public void solicitarLanzamientos(){

        System.out.println("Cantidad de veces que le gustaría lanzar los dados.");
        comprobarInt();
        int lanzamientos = lector.nextInt();

        while (lanzamientos < 1) comprobarDados(lanzamientos);

        lanzamientosDados(lanzamientos);


    }

    public int comprobarDados(int lanzamientos){

        while (lanzamientos < 1){
            System.out.println("El dado se tiene que lanzar una vez, al menos, vuelve a introducir la cantidad, por favor.");
            comprobarInt();
            lanzamientos = lector.nextInt();
        }
        return lanzamientos;
    }

    public void comprobarInt(){
        while (!lector.hasNextInt()){
            System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
            lector.next();
        }
    }

    public void lanzamientosDados(int lanzamientos){
        int dado1, dado2, suma;
        int [] resultadoDados = new int[11];

        for (int i = 0; i<lanzamientos;i++) {

            dado1 = (int) (Math.random() * 7);
            dado2 = (int) (Math.random() * 7);

            while (dado1 == 0 || dado2 == 0) {
                dado1 = (int) (Math.random() * 7);
                dado2 = (int) (Math.random() * 7);
            }

            suma = dado1+dado2;
            resultadoDados[suma-2]++;
        }

        mostrarResultados(resultadoDados);
    }

    public void mostrarResultados(int [] resultadoDados){
        int max=resultadoDados[0], masVeces=0;

        System.out.println("El histograma sería el siguiente: ");
        for (int i=0;i<resultadoDados.length;i++){

            if(i+2 < 10) System.out.print((i+2) + "  = ");

            else System.out.print((i+2) + " = ");

            for (int j=0;j<resultadoDados[i];j++){
                System.out.print("*");
            }

            if (max < resultadoDados[i]){
                max=resultadoDados[i];
                masVeces = (i+2);
            }
            System.out.println();
        }

        System.out.print("El resultado que ha salido más veces es el " + masVeces);
    }
}
