package explicaciones_y_uso_teoría;

public class cerca_binaria_strings_y_recursivo {
    public static void main(String[] args) {

        cerca_binaria_strings_y_recursivo programa = new cerca_binaria_strings_y_recursivo();

        programa.inici();

    }

    public void inici(){
        // NO FUNCIONA, ESTO, LO DEJO AQUÍ PARA SACAR LO QUE PUEDA EN EL EXAMEN, PERO NO LE HE PODIDO SACAR JUGO
    }

    public int busquedaBinariaRecursiva(String[] arreglo, String busqueda, int izquierda, int derecha) {
        // Si izquierda es mayor que derecha significa que no encontramos nada
        if (izquierda > derecha) {
            return -1;
        }

        // Calculamos las mitades...
        int indiceDelElementoDelMedio = (int) Math.floor((izquierda + derecha) / 2);
        String elementoDelMedio = arreglo[indiceDelElementoDelMedio];

        // Primero vamos a comparar y luego vamos a ver si el resultado es negativo,
        // positivo o 0
        int resultadoDeLaComparacion = busqueda.compareTo(elementoDelMedio);

        // Si el resultado de la comparación es 0, significa que ambos elementos son iguales
        // y por lo tanto quiere decir que hemos encontrado la búsqueda
        if (resultadoDeLaComparacion == 0) {
            return indiceDelElementoDelMedio;
        }

        // Si no, entonces vemos si está a la izquierda o derecha
        if (resultadoDeLaComparacion < 0) {
            derecha = indiceDelElementoDelMedio - 1;
            return busquedaBinariaRecursiva(arreglo, busqueda, izquierda, derecha);
        } else {
            izquierda = indiceDelElementoDelMedio + 1;
            return busquedaBinariaRecursiva(arreglo, busqueda, izquierda, derecha);
        }
    }
}
