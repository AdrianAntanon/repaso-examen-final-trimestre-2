package explicaciones_y_uso_teoría;

public class ordenacion_burbuja {
    public static void main (String[] args) {
        float[] llistaNotes = {5.5f, 9f, 2f, 10f, 4.9f};
        for (int i = 0; i < llistaNotes.length - 1; i++) {

            for(int j = i + 1; j < llistaNotes.length; j++) {

                if (llistaNotes[i] > llistaNotes[j]) {

                    float canvi = llistaNotes[i];
                    llistaNotes[i] = llistaNotes[j];
                    llistaNotes[j] = canvi;
                }
            }
        }
        System.out.print("L'array ordenat és: [");
        for (int i = 0; i < llistaNotes.length;i++) {
            System.out.print(llistaNotes[i] + " ");
        }
        System.out.println("]");
    }
}

/*
Inici del bucle	Fi del bucle
‘i’ val	‘j’ val	‘llistaNotes’ val	llistaNotes[i] > llistaNotes[j]	‘llistaNotes’ val
Inici primera iteració bucle extern. Se cerca el valor més petit per posar-lo a la posició 0.
0	1	{5.5, 9, 2, 10, 4.9}	5.5 > 9, false	{5.5, 9, 2, 10, 4.9}
0	2	{5.5, 9, 2, 10, 4.9}	5.5 > 2, true	{2, 9, 5.5, 10, 4.9}
0	3	{2, 9, 5.5, 10, 4.9}	2 > 10, false	{2, 9, 5.5, 10, 4.9}
0	4	{2, 9, 5.5, 10, 4.9}	2 > 4.9, false	{2, 9, 5.5, 10, 4.9}
Fi primera iteració bucle extern. A llistaNotes[0] hi ha el valor més petit.
Inici segona iteració bucle extern. Se cerca el segon valor més petit per posar-lo a la posició 1.
1	2	{2, 9, 5.5, 10, 4.9}	9 > 5.5, true	{2, 5.5, 9, 10, 4.9}
1	3	{2, 5.5, 9, 10, 4.9}	5.5 > 10, false	{2, 5.5, 9, 10, 4.9}
1	4	{2, 5.5, 9, 10, 4.9}	5.5 > 4.9, true	{2, 4.9, 9, 10, 5.5}
Fi segona iteració bucle extern. A llistaNotes[1] hi ha el segon valor més petit.
Inici tercera iteració bucle extern. Se cerca el tercer valor més petit per posar-lo a la posició 2.
2	3	{2, 4.9, 9, 10, 5.5}	9 > 10, false	{2, 4.9, 9 , 10, 5.5}
2	4	{2, 4.9, 9, 10, 5.5}	9 > 5.5, true	{2, 4.9, 5.5, 10, 9}
Fi tercera iteració bucle extern. A llistaNotes[2] hi ha el tercer valor més petit.
Inici quarta iteració bucle extern. Se cerca el quart valor més petit per posar-lo a la posició 3.
3	4	{2, 4.9, 5.5, 10, 9}	10 > 9, true	{2, 4.9, 5.5, 9, 10}
Fi tercera iteració bucle extern. A llistaNotes[3] hi ha el quart valor més petit.
Per força, a la posició 4 hi ha el valor més alt.
4	4	Hem sortit del bucle extern (4 > llistaNotes.length - 1)


 */
