package explicaciones_y_uso_teoría;

import java.util.Scanner;

public class funciones_Math {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        funciones_Math programa = new funciones_Math();
        programa.inici();
    }

    public void inici(){
        valorMaxMin();
        raizCuadrada_Elevar();

    }

    public void valorMaxMin(){
        System.out.println("Calcular valor máximo y mínimo");
        int max = Math.max(10,30);
        int min = Math.min(10,30);

        System.out.println("El valor máximo y valor mínimo entre 10 y 30 es " + max + " = valor náximo y " + min + " = valor mínimo\n" +
                "");
    }

    public void raizCuadrada_Elevar(){
        double base = 4.5, altura = 10;

        double sumaCuadrados = Math.pow(base,2) + Math.pow(altura,2);
        double hipotenusa = Math.sqrt(sumaCuadrados);

        System.out.println("Función para elevar un valor usando Math.pow = 4,5 es " + Math.pow(base,2) + " elevado a 2");
        System.out.println("Para calcular la hipotenusa si tenemos una base de " + base + " y  altura  de " + altura + " lo realizamos con\n" +
                "Math.sqrt y nos da lo siguiente, primer elevariamos a 2 tanto base como altura usando Math.pow y a continuación\n" +
                "la suma de los valores la usaríamos así Math.sqrt(suma) y nos da lo siguiente " + hipotenusa + "\n" +
                "");
    }


}
