package explicaciones_y_uso_teoría;

public class metodos {

    public static void main(String[] args) {
        metodos programa = new metodos();

        programa.inici();
        programa.metode1();
        programa.metode2();
        programa.metode3();

    }

    public void inici(){
        System.out.println("inici");
    }

    public void metode1(){
        System.out.println("metode 1");
    }

    public void metode2(){
        System.out.println("metode 2");
    }

    public void metode3(){
        System.out.println("metode 3");
    }

}

