package explicaciones_y_uso_teoría;

import java.util.Scanner;

public class ordenacion_burbuja_strings {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {

        ordenacion_burbuja_strings programa = new ordenacion_burbuja_strings();
        programa.inici();

    }

    public void inici(){
        int num_palabras;

        System.out.println("¿Cuántas palabras quieres introducir?");
        comprobarInt();
        num_palabras = lector.nextInt();

        String[] palabras= new String[num_palabras];

        palabras = pedirPalabras(palabras);
        mostrarDesordenado(palabras);
        mostrarOrdenado(palabras);
    }

    public void mostrarOrdenado(String [] palabras){

        for(int i=0; i<palabras.length-1; i++) {
            for(int j=i+1; j<palabras.length; j++) {
                if( palabras[i].compareTo( palabras[j] ) > 0 ) {
                    String aux   = palabras[i];
                    palabras[i]  = palabras[j];
                    palabras[j]= aux;
                }
            }
        }

        System.out.println("\n array palabras ordenado: ");
        for(int i=0; i<palabras.length; i++)
        {System.out.println( " palabras[" + i + "] = " + palabras[i] ); }
    }

    public void mostrarDesordenado(String [] palabras){
        System.out.println("array de palabras desordenado: ");
        for(int i=0; i<palabras.length; i++){
            System.out.println( " palabra[" + i + "] = " + palabras[i] );
        }
    }

    public String [] pedirPalabras(String[] palabras){

        for(int i=0; i<palabras.length; i++){
            System.out.println("Introduce palabra "+(i+1)+", por favor");
            palabras[i] = lector.next().toLowerCase();
        }

        return palabras;
    }

    public void comprobarInt(){
        while (!lector.hasNextInt()){
            System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
            lector.next();
        }
    }

}
