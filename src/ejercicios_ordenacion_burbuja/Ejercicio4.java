package ejercicios_ordenacion_burbuja;
/*
    Feu un programa que donats dos vectors inicials de 10 posicions ens retorni un vector ordenat de 20 posicions on siguin tots els valors dels dos vectors inicials.
 */
public class Ejercicio4 {
    public static void main(String[] args) {
        int cambio, contador=0;
        int [] primero = {100, 45, 22, 1112, 245, 22, 44, 1, 2432, -10};
        int [] segundo = {66, 32, 3535, -1, 234, -26, 12, 44, 0, -615};
        int [] union = new int[20];

        for (int i=0;i<union.length;i++){
            if (i<10){
                union[i]=primero[i];
            }else{
                union[i]=segundo[contador];
                contador++;
            }
        }

        for (int i=0;i<union.length;i++){
            for (int j=i+1;j<union.length;j++){
                if (union[i]>union[j]){
                    cambio=union[i];
                    union[i]=union[j];
                    union[j]=cambio;
                }
            }
        }

        System.out.println("Primer vector");
        for (int lista: primero){
            System.out.print(lista + " ");
        }

        System.out.println("\n" +
                "Segundo vector");
        for (int lista: segundo){
            System.out.print(lista + " ");
        }

        System.out.println("\n" +
                "Union de los vectores ordenado de menor a mayor");
        for (int lista: union){
            System.out.print(lista + " ");
        }

    }
}
