package ejercicios_ordenacion_burbuja;
/*
    Feu un programa que donat un vector inicial de 20 posicions ens retorni dos vectors.
    El primer vector haurà de tenir els deu primers números ordenats de menor a major.
    El segon vector haurà de tenir els deu darrers números ordenats també de menor a major.
    */
public class Ejercicio3 {
    public static void main(String[] args){
        int [] vector = {5, -5, 10, 15, 0, 6, 99, 50, -1, 13, -9, -88, 75, 11, 995, 443, -111, 20, 3, 100};
        int [] primero = new int[10];
        int [] segundo = new int[10];
        int i, j, contador=0;

        System.out.println("Array completo");
        for (int lista: vector){
            System.out.print(lista + " ");
        }

        for (i=0;i<vector.length;i++){
            for (j=i+1;j<vector.length;j++){
                if (vector[i]>vector[j]){
                    int cambio = vector[i];
                    vector[i]=vector[j];
                    vector[j]=cambio;
                }
            }
        }

        for (i=0;i<vector.length;i++){
            if (i<10){
                primero[i]=vector[i];
            }else{
                segundo[contador]=vector[i];
                contador++;
            }
        }

        System.out.println("\n" +
                "Primer array ordenado");
        for (int lista: primero){
            System.out.print(lista + " ");
        }

        System.out.println("\n" +
                "Segundo arrya ordenado");
        for (int lista: segundo){
            System.out.print(lista + " ");
        }
    }
}