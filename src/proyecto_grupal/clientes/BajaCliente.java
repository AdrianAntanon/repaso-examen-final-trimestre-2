package proyecto_grupal.clientes;

import java.util.Scanner;

public class BajaCliente {
    Scanner lector = new Scanner(System.in);

    public BajaCliente(String[][] clientes) {
        System.out.println("Introduce el número de cliente del usuario que quiere dar de baja, por favor");
        comprobacion();
        int codCliente = lector.nextInt();
        String codigoCliente = String.valueOf(codCliente);
        boolean comprobacion = true;

        for (int i=0; i<clientes.length;i++){
            if (clientes[i][0] != null){
                if (clientes[i][1].equals(codigoCliente)){
                    clientes[i][0] = "1";
                    comprobacion = false;
                }
            }
        }

        if (comprobacion){
            System.out.println("Lo sentimos, no figura ningún cliente en nuestra BBDD con ese número");
        }else {
            System.out.println("Baja confirmada");
        }

    }

    public void comprobacion(){

        while (!lector.hasNextInt()){
            System.out.println("Datos incorrectos, vuelva a introducirlo, por favor");
            lector.next();
        }
    }

}
