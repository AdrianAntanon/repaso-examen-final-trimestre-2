package proyecto_grupal.clientes;

import java.util.Scanner;

public class AltaCliente {

    int maxclients = 100;
    int[] codigos = new int[maxclients];
    Scanner lector = new Scanner(System.in);

    public AltaCliente(String[][] clientes){

        int codigo = 0;
        boolean ok = false;
        System.out.println("Introduce el número de cliente, por favor");
        while (!ok) {
            comprobacion();
            codigo = lector.nextInt();
            boolean dentroRango = validarCod(codigo);
            if (dentroRango) {
                if (codigos[codigo - 1] == 0) {
                    ok = true;
                    codigos[codigo - 1]++;
                } else System.out.println("Este codigo ya está en uso, introduzca otro, por favor");
            }
        }


        clientes[codigo-1][0] = "0";
        clientes[codigo-1][1] = String.valueOf(codigo);
        clientes[codigo-1][2] = solicitarNIF();
        clientes[codigo-1][3] = solicitarNombre();
        clientes[codigo-1][4] = solicitarApellidos();
        int telefono = solicitarTelefono();
        clientes[codigo-1][5] = String.valueOf(telefono);

    }

    public boolean validarCod(int codigo){
        if (codigo <= 0 || codigo >100) {
            System.out.println("El codigo introducido está fuera del rango [1-100]");
            return false;
        }
        else return true;
    }

    public String solicitarNIF(){

        char [] letras = {'T', 'R', 'W', 'A', 'G', 'H', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};
        int DNI=0, modulo=23;
        String nif = "";
        System.out.println("Introduce el número de NIF sin letra, por favor");
        comprobacion();
        DNI = lector.nextInt();

        while (DNI< 10000000 || DNI>99999999 ){
            System.out.println("Un NIF está formado por 8 números");
            comprobacion();
            DNI = lector.nextInt();
        }

        for (int i=0;i<letras.length;i++){

            if (DNI%modulo == i){
                nif = String.valueOf(DNI);
                nif += letras[i];
            }
        }
        return nif;
    }

    public String solicitarNombre(){
        System.out.println("Introduce tu nombre, por favor");
        String nombre = lector.next();
        lector.nextLine();

        return nombre;
    }

    public String solicitarApellidos(){
        System.out.println("Introduce tus apellidos, por favor");
        String apellidos = lector.next();
        apellidos = apellidos + " " + lector.next();
        lector.nextLine();

        return apellidos;
    }

    public int solicitarTelefono(){

        System.out.println("Introduce tu número de teléfono, por favor");
        comprobacion();
        int tlf = lector.nextInt();
        while (tlf < 100000000 || tlf > 999999999){
            System.out.println("El número debe estar formado por 9 números, vuelve a introducirlo");
            comprobacion();
            tlf = lector.nextInt();
        }

        return tlf;
    }

    public void comprobacion(){

        while (!lector.hasNextInt()){
            System.out.println("Datos incorrectos, vuelva a introducirlo, por favor");
            lector.next();
        }
    }
}

