package proyecto_grupal.clientes;

public class OrdenarClientes {

    public OrdenarClientes(String [][] clientes){
        System.out.println("Ordenando clientes por apellido...");

        for (int i = 0; i < clientes.length; i++) {
            for (int j = i + 1; j < clientes.length; j++) {

                if (clientes[i][0] != null && clientes[j][0] != null) {

                    int resultado = clientes[i][4].compareToIgnoreCase(clientes[j][4]);

                    if (resultado > 0) {
                        String [] cambio = clientes[i];
                        clientes[i] = clientes[j];
                        clientes[j] = cambio;
                    }
                }
            }
        }

        System.out.println("LOADING...\n" +
                "LOADING...\n" +
                "LOADING...\n" +
                "Lista ordenada.");

    }
}
