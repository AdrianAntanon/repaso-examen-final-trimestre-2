package proyecto_grupal.clientes;

public class visualizarCliente {
    final int maxInformacion = 6;

    public visualizarCliente(String[][] clientes) {
        boolean comprobacion = true;

        System.out.println("El listado de clientes es el siguiente: ");

        for (int i=0; i<clientes.length;i++){
            if (clientes[i][0] != null){
                comprobacion = false;
                for (int j=0;j<maxInformacion;j++){
                    System.out.print(clientes[i][j] + " ");
                }
                System.out.println();
            }
        }

        if (comprobacion){
            System.out.println("Lo sentimos, no hay ningún usuario dado de alta todavía.");
        }
    }
}
