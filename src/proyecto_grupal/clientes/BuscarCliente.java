package proyecto_grupal.clientes;

import java.util.Scanner;

public class BuscarCliente {
    final int maxInformacion = 6;
    Scanner lector = new Scanner(System.in);

    public BuscarCliente(String [][] clientes){
        System.out.println("Introduce el número de cliente para visualizarlo, por favor");
        comprobacion();
        int codCliente = lector.nextInt();
        String codigoCliente = String.valueOf(codCliente);

        boolean comprobacion = true;

        for (int i=0; i<clientes.length;i++){
            if (clientes[i][0] != null){
                if (clientes[i][1].equals(codigoCliente)){
                    System.out.println("La información del cliente es el siguiente: ");
                    for (int j=0;j<maxInformacion;j++){
                        System.out.print(clientes[i][j] + " ");
                        comprobacion = false;
                    }
                    System.out.println();
                }
            }
        }

        if (comprobacion){
            System.out.println("Lo sentimos, no figura ningún cliente en nuestra BBDD con ese número");
        }
    }

    public void comprobacion(){

        while (!lector.hasNextInt()){
            System.out.println("Datos incorrectos, vuelva a introducirlo, por favor");
            lector.next();
        }
    }
}
