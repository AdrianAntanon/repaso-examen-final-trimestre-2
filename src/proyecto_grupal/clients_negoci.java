package proyecto_grupal;
import proyecto_grupal.clientes.*;

import java.util.Scanner;

    /*
Realitzar un programa per a la gestió de l’alta de clients en les dades del nostre negoci.
Abans de començar a implementar el programa, per tal d’organitzar les dades de cada client, dissenyarem un tuple (Strings) que contingui les dades d’un client.

El tuple contindrà com a mínim 6 camps (esborrat, codi, nif, nom, cognoms, telèfon,...).

El programa podrà donar d’alta com a màxim 100 clients.

Els camps esborrat i codi i seran enters; els codis acceptats seran de l’1 al 100.
El camp esborrat indicarà amb 0/1 si el client ha estat esborrat (aquest valor NO l’introdueix l’usuari: el controlarà el propi programa).

El programa estarà organitzat per un menú:

Alta d’un client
Visualització d’un client
Baixa d’un client
Recuperació de dades de client
Ordena els clients
Recerca d’un client
Sortir


Les funcions a implementar son mínim una per cada opció de menú menys la opció sortir.

Haureu de validar que el codi estigui entre el rang 0-100 amb una funció que ens retornarà un 0 o 1 en funció del resultat (rep un possible codi de client i si és vàlid, retorna 1; si no, retorna 0).
Ell client es marca com a esborrat (esborrat=1)
Recupera les dades del client, si estava esborrat, el recupera (esborrat=0)
Ordena client ordena el vector pel camp cognoms
Recerca:Busca un client utilitzant el mètode de recerca binaria.

     */


public class clients_negoci {
    final int maxclients = 100;
    final int maxInformacion = 6;

    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {

        clients_negoci programa = new clients_negoci();
        programa.inici();

    }

    public void inici(){
        int seleccion=0, i=0;
        String [][] clientes = new String[maxclients][maxInformacion];

        do {
            menu();
            comprobacion();
            seleccion = lector.nextInt();

            switch (seleccion){
                case 1:
                    new AltaCliente(clientes);
                    break;
                case 2:
                    new visualizarCliente(clientes);
                    break;
                case 3:
                    new BajaCliente(clientes);
                    break;
                case 4:
                    new RecuperarCliente(clientes);
                    break;
                case 5:
                    new OrdenarClientes(clientes);
                    break;
                case 6:
                    new BuscarCliente(clientes);
                    break;
                case 7:
                    System.out.println("Saliendo del programa.");
                    break;
                default:
                    System.out.println("Opción no válida");
            }

        }while (seleccion != 7);
    }

    public void menu(){
        System.out.println("Seleccione la opción que desea, por favor:\n" +
                "1) Dar de alta\n" +
                "2) Visualizar clientes\n" +
                "3) Dar de baja \n" +
                "4) Recuperar datos de cliente\n" +
                "5) Ordenar clientes\n" +
                "6) Buscar cliente\n" +
                "7) Salir");
    }

    public void comprobacion(){

        while (!lector.hasNextInt()){
            System.out.println("Datos incorrectos, vuelva a introducirlo, por favor");
            lector.next();
        }
    }
}
