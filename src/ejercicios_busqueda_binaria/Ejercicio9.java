package ejercicios_busqueda_binaria;

import java.util.Scanner;

/*
Feu un programa que demani 10 números (DIFERENTS) pel teclat i els emmagatzemi a dintre d'un vector de deu posicions.
Haureu de comprovar que cada numero que l'usuari entri pel teclat no sigui ja a dintre del vector.
*/
public class Ejercicio9 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        Ejercicio9 programa = new Ejercicio9();
        programa.inici();
    }

    public void inici(){
        int [] numeros = new int[10];
        for (int i=numeros.length-1;i>=0;i--){

            int valor = pedirValor(i);

            busquedaBinaria(valor,numeros, i);

            numeros[i]=valor;

            bubbleSort(numeros);
        }

        resultado(numeros);
    }

    public int pedirValor(int i){
        System.out.println("Introduce el valor " + (10-i));

        comprobacion();

        int valor=lector.nextInt();
        return valor;
    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("Número no válido, vuelve a introducirlo");
            lector.next();
        }
    }

    public void busquedaBinaria(int valor, int [] numeros, int i){
        int busquedaAbajo=0, busquedaArriba=numeros.length-1;
        int posicion;
        boolean trobat=true;

        while(busquedaAbajo<=busquedaArriba && trobat){

            posicion= (busquedaArriba+busquedaAbajo)/2;

            if (numeros[posicion]==valor) trobat=false;
            else if (valor <numeros[posicion]) busquedaArriba=posicion-1;
            else busquedaAbajo=posicion+1;

            while (!trobat){
                System.out.println("El número ya figura dentro del vector, vuelva a introducir el valor " + (10-i) + ", por favor");
                comprobacion();
                valor=lector.nextInt();

                trobat=!trobat;
                busquedaAbajo=0;
                busquedaArriba=numeros.length-1;
            }
        }

    }

    public void bubbleSort(int [] numeros){
        for (int x = 0;x < numeros.length-1;x++) {

            for(int j = x + 1; j < numeros.length; j++) {

                if (numeros[x] > numeros[j]) {

                    int canvi = numeros[x];
                    numeros[x] = numeros[j];
                    numeros[j] = canvi;
                }
            }
        }
    }

    public void resultado(int [] numeros){
        System.out.println("El vector quedaría de la siguiente forma");
        for (int lista: numeros){
            System.out.print(lista+ " ");
        }
    }

}
