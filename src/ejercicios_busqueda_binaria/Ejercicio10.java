package ejercicios_busqueda_binaria;
/*
 Dissenya un programa que demani els ingressos que en Joan i en Pere han tingut durant els dotze mesos de l'any
 (tenint en compte que ni en Joan ni en Pere poden cobrar la mateixa quantitat d’euros en mesos diferents (com en l’exercici anterior)
 i els emmagatzemi i ordeni en dos vectors (un per Joan i l'altre per Pere).

 Una vegada ordenats i utilitzant el mètode de cerca binària ens ha de dir quants de mesos en Joan i Pere han cobrat la mateixa quantitat.

 SÍ QUE PUEDEN REPETIR LO QUE COBRAN JOAN Y PERE, LO GUARDO EN UNA ARRAY Y ENTONCES REALIZO UNA BÚSQUEDA BINARIA PARA VER CUÁNTOS MESES HAN COBRADO LO MISMO
 */

import java.util.Scanner;

public class Ejercicio10 {
    public static void main(String[] args) {

        Scanner lector = new Scanner (System.in);
        double[] Joan = new double[3];
        double[] Pere = new double[3];
        int busquedaAbajo = 0, busquedaArriba = Joan.length - 1, posicion = 0, i;
        String [] nombres = {"Joan","Pere"};
        boolean trobat=true;

        for (i=Joan.length-1;i>=0;i--){

            System.out.println("Introduce los ingresos que ha tenido " + nombres[0] + " el mes " + (3-i) + ", por favor");

            while (!lector.hasNextDouble()){
                System.out.println("No es un número válido, vuelva a introducirlo, por favor");
                lector.next();
            }

            double balanceMensual = lector.nextDouble();

            while (busquedaAbajo <= busquedaArriba && trobat) {

                posicion = (busquedaArriba + busquedaAbajo) / 2;

                if (Joan[posicion] == balanceMensual) trobat = false;
                else if (balanceMensual < Joan[posicion]) busquedaArriba = posicion - 1;
                else busquedaAbajo = posicion + 1;

                while (!trobat) {
                    System.out.println("El número ya figura dentro del vector, vuelva a introducir el mes " + (i+1) + ", por favor");
                    while (!lector.hasNextDouble()) {
                        System.out.println("Número no válido");
                        lector.next();
                    }
                    balanceMensual = lector.nextDouble();

                    trobat = !trobat;
                    busquedaAbajo = 0;
                    busquedaArriba = Joan.length - 1;
                }
            }

            Joan[i] = balanceMensual;
            busquedaAbajo = 0;
            busquedaArriba = Joan.length - 1;

            for (int x = 0; x < Joan.length- 1; x++) {

                for(int z = x + 1; z < Joan.length; z++) {

                    if (Joan[x] > Joan[z]) {
                        double canvi = Joan[x];
                        Joan[x] = Joan[z];
                        Joan[z] = canvi;
                    }
                }
            }

        }
        System.out.println("Joan");
        for (i=0;i<Joan.length;i++){
            System.out.print(Joan[i] + " ");
        }

        System.out.println();

        for (i=Pere.length-1;i>=0;i--){

            System.out.println("Introduce los ingresos que ha tenido " + nombres[1] + " el mes " + (3-i) + ", por favor");

            while (!lector.hasNextDouble()){
                System.out.println("No es un número válido, vuelva a introducirlo, por favor");
                lector.next();
            }

            double balanceMensual = lector.nextDouble();

            while (busquedaAbajo <= busquedaArriba && trobat) {

                posicion = (busquedaArriba + busquedaAbajo) / 2;

                if (Pere[posicion] == balanceMensual) trobat = false;
                else if (balanceMensual < Pere[posicion]) busquedaArriba = posicion - 1;
                else busquedaAbajo = posicion + 1;

                while (!trobat) {
                    System.out.println("El número ya figura dentro del vector, vuelva a introducir el mes " + (i+1) + ", por favor");
                    while (!lector.hasNextDouble()) {
                        System.out.println("Número no válido");
                        lector.next();
                    }
                    balanceMensual = lector.nextDouble();

                    trobat = !trobat;
                    busquedaAbajo = 0;
                    busquedaArriba = Pere.length - 1;
                }
            }

            Pere[i] = balanceMensual;
            busquedaAbajo = 0;
            busquedaArriba = Pere.length - 1;

            for (int x = 0; x < Pere.length- 1; x++) {

                for(int z = x + 1; z < Pere.length; z++) {

                    if (Pere[x] > Pere[z]) {
                        double canvi = Pere[x];
                        Pere[x] = Pere[z];
                        Pere[z] = canvi;
                    }
                }
            }
        }
        System.out.println("Pere");
        for (i=0;i<Pere.length;i++){
            System.out.print(Pere[i] + " ");
        }

    }
}

