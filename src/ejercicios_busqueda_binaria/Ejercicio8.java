package ejercicios_busqueda_binaria;

import java.util.Scanner;

/*
Fer un programa que demani per teclat les següents dades:  Els ingressos que Joan ha rebut durant els dotze mesos de l'any.
 La quantitat a cercar a dintre del vector. Com a resultat de la cerca el programa ens ha de dir si Joan ha rebut o no aquesta quantitat,
 i en cas positiu, ens digui en quin mes l'ha cobrat. Nota: Haureu de modificar el resultat la funció de cerca seqüencial.
 Aquesta funció haurà de retornar el següent: 0: Si no hem trobat el ingrés a dintre del vector. i+1 (el mes): Si trobem el ingrés a dins del vector.

*/
public class Ejercicio8 {
    Scanner lector = new Scanner(System.in);


    public static void main(String[] args) {
        Ejercicio8 programa = new Ejercicio8();
        programa.inici();
    }

    public void inici(){

        double [] ingresos = introduccionIngresos();

        binarySearch(ingresos);
    }

    public double [] introduccionIngresos(){
        double[] ingresos = new double[3];

        for (int i=0;i<ingresos.length;i++){
            System.out.println("Introduce los ingresos del mes "+ (i+1) +", por favor");
            comprobacion();
            ingresos[i]=lector.nextDouble();
        }
        return ingresos;
    }

    public void comprobacion(){
        while (!lector.hasNextDouble()){
            System.out.println("Eso no es un número válido");
            lector.next();
        }
    }

    public double cantidad_a_buscar(){

        System.out.println("Ahora introduce el cantidad a buscar");
        comprobacion();
        double valor = lector.nextDouble();

        return valor;
    }

    public void binarySearch(double[] ingresos){

        double valor = cantidad_a_buscar();

        int busquedaAbajo=0, busquedaArriba=ingresos.length-1, posicion=0, trobat=0;

        while(busquedaAbajo<=busquedaArriba && trobat==0){

            posicion= (busquedaArriba+busquedaAbajo)/2;

            if (ingresos[posicion]==valor) trobat=1;
            else if (valor <ingresos[posicion]) busquedaArriba=posicion-1;
            else busquedaAbajo=posicion+1;
        }

        if (trobat==1) System.out.println("La cantidad de " + valor + "€ se ha encontrado el mes " + (posicion+1));
        else System.out.println("0, no se ha encontrado la cantidad introducida");
    }
}
