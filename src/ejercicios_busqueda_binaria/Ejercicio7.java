package ejercicios_busqueda_binaria;
import java.util.Scanner;

/*
Feu a ma els diferents passos que hauria de fer l'algorisme de cerca dicotòmica al buscar a dintre del vector (10, 25, 31, 46, 52, 63, 71, 84, 91, 92) els valors 84 i l'1.
*/
public class Ejercicio7 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        Ejercicio7 programa = new Ejercicio7();
        programa.inici();
    }

    public void inici() {

        System.out.println("Introduce el valor a buscar, por favor");

        binarySearch();

    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("Eso no es un número entero");
            lector.next();
        }
    }

    public void binarySearch(){

        comprobacion();
        int valor = lector.nextInt();

        int trobat=0;
        int [] valores ={10, 25, 31, 46, 52, 63, 71, 84, 91, 92};
        int busquedaAbajo=0, busquedaArriba=valores.length-1, posicion;

        while(busquedaAbajo<=busquedaArriba && trobat==0){

            posicion= (busquedaArriba+busquedaAbajo)/2;

            if (valores[posicion]==valor) trobat=1;
            else if (valor <valores[posicion]) busquedaArriba=posicion-1;
            else busquedaAbajo=posicion+1;
        }

        resultado(trobat);
    }

    public void resultado(int trobat){

        if (trobat==1) System.out.println("S'ha trobat");
        else System.out.println("No s'ha trobat");
    }
}
