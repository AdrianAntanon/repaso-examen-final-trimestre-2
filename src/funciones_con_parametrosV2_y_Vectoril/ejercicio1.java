package funciones_con_parametrosV2_y_Vectoril;

import java.util.Scanner;

/*
Implementa una funció que rebi un int i torni el seu quadrat.
*/
public class ejercicio1 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio1 programa = new ejercicio1();
        programa.inici();
    }

    public void inici(){
        imprimirResultado(calculoCuadrado(solicitudDatos()));
    }

    public int solicitudDatos(){
        System.out.println("Introduce un número entero y se devolverá su cuadrado");
        comprobacion();
        int num = lector.nextInt();

        return num;
    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("Eso no es un número entero, vuelve a introducirlo, por favor");
            lector.next();
        }
    }

    public int calculoCuadrado(int a){

        a = a*a;

        return a;
    }
    public void imprimirResultado(int a){

        System.out.println("El cuadrado del valor introducido es " + a);
    }
}
