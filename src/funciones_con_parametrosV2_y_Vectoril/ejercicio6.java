package funciones_con_parametrosV2_y_Vectoril;

import java.util.Scanner;

/*
Dissenya un programa que calculi la longitud en caràcters d’una paraula entrada per l’usuari. Hauràs de fer un procediment que faci el càlcul de la longitud de paraules.
*/
public class ejercicio6 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio6 programa = new ejercicio6();
        programa.inici();
    }

    public void inici(){
        resultado(solicitud());
    }

    public void resultado(int a){

        System.out.println("La palabra introducida tiene " + a + " carácteres");

    }

    public int solicitud(){
        System.out.println("Introduce una palabra, por favor");

        String palabra=lector.next();

        int longitud = palabra.length();

        return longitud;
    }
}
