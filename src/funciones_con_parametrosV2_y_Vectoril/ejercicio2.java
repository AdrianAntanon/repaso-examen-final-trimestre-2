package funciones_con_parametrosV2_y_Vectoril;

import java.util.Scanner;

/*
    Dissenya una funció que rebi un float i torni el seu quadrat
*/
public class ejercicio2 {
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        ejercicio2 programa = new ejercicio2();
        programa.inici();
    }

    public void inici(){
        imprimirResultado(calculoCuadrado(solicitudDatos()));
    }

    public float solicitudDatos(){
        System.out.println("Introduce un número y se devolverá su cuadrado");
        comprobacion();
        float num = lector.nextFloat();

        return num;
    }

    public void comprobacion(){
        while (!lector.hasNextFloat()){
            System.out.println("Eso no es un número, vuelve a introducirlo, por favor");
            lector.next();
        }
    }

    public float calculoCuadrado(float a){

        a = a*a;

        return a;
    }
    public void imprimirResultado(float a){
        System.out.printf("El cuadrado del valor introducido es %.2f" , a);
    }
}
