package funciones_con_parametrosV2_y_Vectoril;

import java.util.Scanner;

/*
Objectius:
Codificar funcions.
Estructurar un programa en forma modular.

Enunciat:
La pràctica consisteix en implementar una aplicació mitjançant funcions.
El programa en java que gestionarà la realització de determinades operacions en un vector d’enters de MAX (6, per exemple) posicions:

Introducció de tots els elements del vector (Altes).
Visualització de tots els elements (Llistat).
Visualització de l’element que es troba en una determinada posició (Consulta per Posició).
Visualitzar la suma total dels elements del vector ( Suma )
Visualitzar la mitjana aritmètica dels elements del vector ( Mitjana )
Visualitzar el valor màxim, mínim i la mitjana ( MaxMitMin )

Cal dissenyar el programa de forma modular, de manera que cada funció s’encarregui de manipular les dades de la forma apropiada.
El vector s’ha d’enviar com a paràmetre a totes les funcions, fer validació de dades allà on faci falta.
Se'ns exigeix que hi hagi com a mínim les següents accions i funcions:

Funció InicializarVector ( tots els valors a 0 )
Introduir valors
Funció VisualitzarMenu
Funció Llistat
Funció ConsultarPosicio
Funció Suma
Funció Mitjana
Funció MaxMitMin

*/
public class vectoril {
    final int MAXPOS=6;
    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {
        vectoril programa = new vectoril();
        programa.inici();
    }

    public void inici(){
        despleglable();
    }


    public void despleglable(){
        int seleccion=0;
        int [] vector = iniciarVector();

        do {
            menu();
            comprobacion();
            seleccion = lector.nextInt();

            switch (seleccion){
                case 1:
                    vector = valores(vector);
                    break;
                case 2:
                    listado(vector);
                    break;
                case 3:
                    sumaTotal(vector);
                    break;
                case 4:
                    mediana(vector);
                    break;
                case 5:
                    maxmedmin(vector);
                    break;
                case 6:
                    posicionValor(vector);
                    break;
                case 7:
                    System.out.println("Saliendo del programa.");
                    break;
                default:
                    System.out.println("Opción no válida");
            }

        }while (seleccion != 7);

    }

    public void menu(){
        System.out.println("Seleccione la opción que desea, por favor:\n" +
                "1) Introducir valores del vector\n" +
                "2) Visualizar todos los valores\n" +
                "3) Calcular suma total de valores del vector\n" +
                "4) Calcular media aritmética\n" +
                "5) Visualizar valor máximo, mínimo y media aritmética\n" +
                "6) Comprobar el valor de una posición\n" +
                "7) Salir");
    }

    public void comprobacion(){
        while (!lector.hasNextInt()){
            System.out.println("No es un número válido, vuelve a introducirlo, por favor");
            lector.next();
        }
    }

    public int [] iniciarVector(){
        int [] vector = new int[MAXPOS];
        return vector;
    }

    public int[] valores(int [] a){
        for (int i=0;i<a.length;i++){
            System.out.print("Introduce el valor " + (i+1)+" ");
            comprobacion();
            a[i]=lector.nextInt();
        }
        return a;
    }

    public void listado(int [] a){

        System.out.println("Los valores son los siguientes:");
        for (int lista: a){
            System.out.print(lista + " ");
        }
        System.out.println();
    }

    public void sumaTotal(int [] a){
        int total=suma(a);
        System.out.println("La suma total es " + total);
    }

    public int suma(int [] a){
        int resultado=0;

        for (int i=0;i<a.length;i++){
            resultado = resultado + a[i];
        }
        return resultado;
    }

    public void mediana(int [] a){
        double mediana = suma(a)/MAXPOS;

        System.out.printf("La media aritmética es %.2f" , mediana);
        System.out.println();
    }

    public void maxmedmin(int [] a){

        int[] b = new int[MAXPOS];
        for (int i=0;i<a.length;i++){
            b[i]=a[i];
        }

        for (int i = 0; i < b.length - 1; i++) {
            for(int j = i + 1; j < b.length; j++) {
                if (b[i] > b[j]) {
                    int canvi = b[i];
                    b[i] = b[j];
                    b[j] = canvi;
                }
            }
        }
        System.out.println("El mínimo es " + b[0] + " y el máximo es " + b[5]);
        mediana(b);

    }

    public void posicionValor (int [] a){
        System.out.println("Qué posición le gustaría comprobar");
        comprobacion();
        int posicion = lector.nextInt();
        while (posicion > MAXPOS || posicion < 1){
            System.out.println("Introduzca la posición del 1 al 10, por favor");
            comprobacion();
            posicion=lector.nextInt();
        }

        System.out.println("El número que figura en la posición " + posicion + " es el " + a[posicion-1]);

    }

}
