package funciones_con_parametrosV2_y_Vectoril;
/*
Quina errada trobes en aquesta funció?
*/
public class ejercicio5 {
    public static void main(String[] args) {
        ejercicio5 programa = new ejercicio5();
        programa.inici();
        /*
            El error que encuentro es que solo está declarado el primer int, b y c no, por lo que daría error.
                 int minim (int a, b, c){
                 if (a <= b && a <= c)
                      return a;
                 if (b <= a && b <= c)
                      return b;
                 return c;
         */

    }

    public void inici(){
        imprimir(minim(1, 3, 5 ));
    }

    public int minim(int a, int b, int c){

        if (a <= b && a <= c) return a;
        if (b <= a && b <= c) return b;

        return c;
    }

    public void imprimir(int a){
        System.out.println(a);
    }
}
