import java.util.Scanner;

public class examen_Ejercicio1 {

    Scanner lector = new Scanner(System.in);
    final int TERRENO_OCUPADO = 1;

    public static void main(String[] args) {
        examen_Ejercicio1 programa = new examen_Ejercicio1();

        programa.inici();
    }

    public void inici(){
        int [][] terreno = new int[10][20];

        terreno = llenarTerreno(terreno);

        pedirCoordenadas(terreno);

    }

    public int [][] llenarTerreno(int [][] terreno){

        int num_arboles = 0;
        System.out.print("      ");
        for (int i=0;i<terreno.length;i++){

            int primera_posicion = (int) (Math.random() * terreno.length);
            int segunda_posicion = (int) (Math.random() * terreno[i].length);

            if (num_arboles < 10){
                terreno[primera_posicion][segunda_posicion] = TERRENO_OCUPADO;
                num_arboles++;
            }

            for (int j=0;j<terreno[i].length;j++){
                if (i == 0){
                    if (j < 9){
                        System.out.print((j+1)+"     ");
                    }else {
                        System.out.print((j+1)+"    ");
                    }
                }
            }

        }
        System.out.println();

        for (int i=0;i<terreno.length;i++){
            if ((i+1) == 10){
                System.out.print((i+1)+"  ");
            }else {
                System.out.print((i+1)+"   ");
            }
            for (int j=0;j<terreno[i].length;j++){
                System.out.print("| ");
                if (terreno[i][j] == 1){
                    System.out.print("+");
                }else {
                    System.out.print(" ");
                }
                System.out.print(" | ");
            }
            System.out.println();
        }

        return terreno;
    }

    public void pedirCoordenadas(int [][] terreno){

        System.out.println("Introduces las coordenadas, por favor\n" +
                "Fila A:");
        comprobacion();
        int posicion_fila_A = lector.nextInt();
        posicion_fila_A = comprobacionFila(posicion_fila_A)-1;

        System.out.println("Columna A:");
        comprobacion();
        int posicion_columna_A = lector.nextInt();
        posicion_columna_A = comprobacionColumna(posicion_columna_A)-1;

        System.out.println("Fila B: ");
        comprobacion();
        int posicion_fila_B = lector.nextInt();
        posicion_fila_B = comprobacionFila(posicion_fila_B)-1;

        System.out.println("Columna B: ");
        comprobacion();
        int posicion_columna_B = lector.nextInt();
        posicion_columna_B = comprobacionColumna(posicion_columna_B)-1;

        contarArboles(terreno, posicion_fila_A, posicion_columna_A, posicion_fila_B, posicion_columna_B);

    }

    public void comprobacion(){

        while (!lector.hasNextInt()){
            System.out.println("No es un número válido, vuelve a introducirlo, por favor");
            lector.next();
        }

    }

    public int comprobacionFila(int fila){

        while (fila < 1 || fila > 10){
            System.out.println("El número no es válido, debe ser mayor que 1 y menor o igual que 10");
            comprobacion();
            fila = lector.nextInt();
        }

        return fila;

    }

    public int comprobacionColumna(int columna){

        while (columna < 1 || columna > 20){
            System.out.println("El número no es válido, debe ser mayor que 1 y menor o igual que 20");
            comprobacion();
            columna = lector.nextInt();
        }

        return columna;

    }

    public void contarArboles(int [][] terreno,int filaA, int columnaA, int filaB, int columnaB){

        int numero_de_arboles = 0;

        System.out.print("El número de arboles entre las coordenadas que has dado es de: ");

        for (int i=0;i<terreno.length;i++){
            for (int j=0;j<terreno[i].length;j++){
                if (i == filaA || i == filaB){
                    if ( j >= columnaA || j== columnaB){
                        if (terreno[i][j] == 1){
                            numero_de_arboles++;
                        }
                    }
                }
            }
        }

        if (numero_de_arboles == 0){
            System.out.println(0 + " árboles");
        }else if (numero_de_arboles == 1){
            System.out.println(numero_de_arboles + " árbol");
        }else {
            System.out.println(numero_de_arboles + " árboles");
        }

    }


}
